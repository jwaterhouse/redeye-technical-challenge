#!/usr/bin/env php
<?php

require __DIR__.'/vendor/autoload.php';

use Jon\MyApplication;

$application = new MyApplication();
$application->run();
