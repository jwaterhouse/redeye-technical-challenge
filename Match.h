#ifndef __MATCH_H__
#define __MATCH_H__

#include <string>
#include <opencv2/opencv.hpp>

// Data structure that stores the information for a matched
// symbol.
class Match
{
public:
    // ctor
    Match(const std::string &   name,
          const cv::Point2i &   location,
          const cv::Rect &      bounds,
          float                 result,
          double                rotation,
          double                scale);

    // dtor
    ~Match();

    // Getters and setters
    const std::string & name() const;
    void name(std::string name);

    const cv::Point2i & location() const;
    void location(const cv::Point2i & location);

    const cv::Rect & bounds() const;
    void bounds(const cv::Rect & bounds);

    float result() const;
    void result(float result);

    double rotation() const;
    void rotation(double rotation);

    double scale() const;
    void scale(double scale);
    
private:
    // Name of the symbol for this match
    std::string m_name;

    // Location within the test image of this match
    cv::Point2i m_location;

    // Bounding rectangle for this match
    cv::Rect    m_bounds;

    // The result value pf the match
    float       m_result;

    // The rotation applied to the original symbol image
    double      m_rotation;

    // The scale applied to the original symbol image
    double      m_scale;
};

#endif