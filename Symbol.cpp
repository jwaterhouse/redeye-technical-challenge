#include "Symbol.h"
#include "thinning.h"

// ctor
Symbol::Symbol()
{
}

Symbol::Symbol(const std::string &  name,
               const cv::Mat &      symbol_sheet,
               const cv::Rect &     roi,
               double               threshold,
               const cv::Range &    scale) :
               m_name(name),
               m_image(symbol_sheet, roi),
               m_threshold(threshold),
               m_scale(scale)
{
}

// dtor
Symbol::~Symbol()
{
}

// Getter and setter for name
const std::string &
Symbol::name() const
{
    return m_name;
}
void
Symbol::name(const std::string & name)
{
    m_name = name;
}

// Getter and setter for image
const cv::Mat &
Symbol::image() const
{
    return m_image;
}
void
Symbol::image(const cv::Mat & image)
{
    image.copyTo(m_image);
}

// Getter and setter for threshold
double
Symbol::threshold() const
{
    return m_threshold;
}
void 
Symbol::threshold(double threshold)
{
    m_threshold = threshold;
}

// Getter and setter for scale
const cv::Range &
Symbol::scale() const
{
    return m_scale;
}
void
Symbol::scale(const cv::Range & scale)
{
    m_scale.start = scale.start;
    m_scale.end = scale.end;
}
