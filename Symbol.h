#ifndef __SYMBOL_H__
#define __SYMBOL_H__

#include <string>
#include <opencv2/core/core.hpp>

// Data structure to store information about a Symbol
class Symbol
{
public:
    // ctor
    Symbol();

    // Construct a Symbol object with the given data.
    // The roi is given as a subregion within the symbol_sheet that
    // is the location of the individual symbol.
    Symbol(const std::string &  name,
           const cv::Mat &      symbol_sheet,
           const cv::Rect &     roi,
           double               threshold,
           const cv::Range &    scale);
    
    // dtor
    ~Symbol();

    // Getter and setter for name
    const std::string & name() const;
    void name(const std::string & name);

    // Getter and setter for image
    const cv::Mat & image() const;
    void image(const cv::Mat & image);

    // Getter and setter for threshold
    double threshold() const;
    void threshold(double threshold);

    // Getter and setter for scale
    const cv::Range & scale() const;
    void scale(const cv::Range & scale);

protected:
    // Name of the symbol
    std::string m_name;

    // Image containing the symbol
    cv::Mat m_image;

    // Threshold to use when determining match in a test image
    double m_threshold;

    // Range of sizes/scales to iterate through when
    // matching against the test image
    cv::Range m_scale;
};

#endif