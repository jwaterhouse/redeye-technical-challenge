<?php
// File: src/Jon/MyCommand.php

namespace Jon;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;

class MyCommand extends Command
{
    protected function configure()
    {
        $this->setName('my_command');
        $this->setDescription('Executes the command.');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //$process = new Process('ls -lsa');
        $process = new Process('./main');
        $process->start();

        while($process->isRunning()) {
            sleep(1);
            echo $process->getIncrementalOutput();
        }
        echo $process->getIncrementalOutput();

        if (!$process->isSuccessful()) {
             throw new \RuntimeException($process->getErrorOutput());
        }
    }
}
