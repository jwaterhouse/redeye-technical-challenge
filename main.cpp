#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <limits>
#include <thread> 
#include <time.h>
#include <opencv2/opencv.hpp>
#include <opencv2/nonfree/features2d.hpp>
//#include <opencv2/core/core.hpp>
//#include <opencv2/highgui/highgui.hpp>
//#include <opencv2/features2d/features2d.hpp>

#include "Symbol.h"
#include "Match.h"
#include "thinning.h"

/*
 *@brief rotate image by factor of 90 degrees
 *
 *@param source : input image
 *@param dst : output image
 *@param angle : factor of 90, even it is not factor of 90, the angle
 * will be mapped to the range of [-360, 360].
 * {angle = 90n; n = {-4, -3, -2, -1, 0, 1, 2, 3, 4} }
 * if angle bigger than 360 or smaller than -360, the angle will
 * be map to -360 ~ 360.
 * mapping rule is : angle = ((angle / 90) % 4) * 90;
 *
 * ex : 89 will map to 0, 98 to 90, 179 to 90, 270 to 3, 360 to 0.
 *
 */
void rotate_image_90n(cv::Mat &src, cv::Mat &dst, int angle)
{   
   if(src.data != dst.data){
       src.copyTo(dst);
   }

   angle = ((angle / 90) % 4) * 90;

   //0 : flip vertical; 1 flip horizontal
   bool const flip_horizontal_or_vertical = angle > 0 ? 1 : 0;
   int const number = std::abs(angle / 90);          

   for(int i = 0; i != number; ++i){
       cv::transpose(dst, dst);
       cv::flip(dst, dst, flip_horizontal_or_vertical);
   }
}

// Detects regions of text within the given image
// img: the image to detect text within
// returns: a vector of bounding rectangles for detected text
std::vector<cv::Rect>
detect_letters(cv::Mat img)
{
    std::vector<cv::Rect> boundRect;
    cv::Mat img_gray, img_sobel, img_threshold, element;
    cvtColor(img, img_gray, CV_BGR2GRAY);
    cv::Sobel(img_gray, img_sobel, CV_8U, 1, 0, 3, 1, 0, cv::BORDER_DEFAULT);
    cv::threshold(img_sobel, img_threshold, 0, 255, CV_THRESH_OTSU+CV_THRESH_BINARY);
    element = getStructuringElement(cv::MORPH_RECT, cv::Size(10, 10) );
    cv::morphologyEx(img_threshold, img_threshold, CV_MOP_CLOSE, element); //Does the trick
    std::vector< std::vector< cv::Point> > contours;
    cv::findContours(img_threshold, contours, 0, 1); 
    std::vector<std::vector<cv::Point> > contours_poly( contours.size() );
    for( int i = 0; i < contours.size(); i++ )
        if (contours[i].size()>100)
        { 
            cv::approxPolyDP( cv::Mat(contours[i]), contours_poly[i], 3, true );
            cv::Rect appRect( boundingRect( cv::Mat(contours_poly[i]) ));
            if (appRect.width>appRect.height) 
                boundRect.push_back(appRect);
        }
    return boundRect;
}

// Computes the matches for a given image and template
// image: the image to search for matches
// templ: the template image to match against
// result: the result of the matching process
void
get_matches(const cv::Mat & image,
             const cv::Mat & templ,
             cv::Mat & result)
{
    // Create the result matrix
    int result_cols =  image.cols - templ.cols + 1;
    int result_rows = image.rows - templ.rows + 1;
    result.create( result_cols, result_rows, CV_32FC1 );

    // Do the Matching and Normalize
    int match_method = CV_TM_CCORR_NORMED;
    cv::matchTemplate(image, templ, result, match_method);
    //cv::normalize(result, result, 0, 1, cv::NORM_MINMAX, -1, cv::Mat());
}

// Handler to calculate the matches for a given test image and symbol.
// Also iterates through all sizes/scales and rotations of the symbol
// image.
// image_test: the thinned test image
// symbol: the Symbol data and image
// matches: a vector of matches to be populated by the algorithm
void
symbol_match_all_handler(const cv::Mat & image_test,
                         const Symbol & symbol,
                         std::vector<Match> & matches)
{
    // Iterate through different sizes of the symbol, in an
    // effort to get some pseudo scale invariance
    for (int scale = symbol.scale().start; scale <= symbol.scale().end; ++scale)
    {
        // Iterate through different rotations of the image
        // i.e. 0, 90, 180, and 270 degrees
        for (double rotation = 0.0; rotation < 360.0; rotation += 90.0)
        {
            cv::Mat image_symbol;

            // Resize the original symbol image
            cv::resize(symbol.image(), image_symbol, cv::Size(),
                scale / 100.0, scale / 100.0);

            // Invert and thin the image
            thinning_special_with_inversion(image_symbol, image_symbol);

            // Rotate the image
            rotate_image_90n(image_symbol, image_symbol, rotation);

            // Retreive matches
            cv::Mat result;
            get_matches(image_test, image_symbol, result);

            // Iterate through all the matches
            for (int c = 0; c < result.cols; ++c)
            {
                for (int r = 0; r < result.rows; ++r)
                {
                    // Check the value and save the match if over the threshold
                    float value = result.at<float>(r, c);
                    if (value >= symbol.threshold())
                    {
                        matches.push_back(
                            Match(symbol.name(),
                            cv::Point2i(c, r),
                            cv::Rect(c, r, image_symbol.cols, image_symbol.rows),
                            value,
                            rotation,
                            scale));

                        std::cout << "\t\tMatch! " << symbol.name();
                        std::cout << "\tscale=" << scale << "%";
                        std::cout << "\trotation=" << rotation;
                        std::cout << "\tvalue=" << value;
                        std::cout << "\tlocation=(" << c << ", " << r << ")" << std::endl;
                    }
                }
            }
        }
    }
}

// Attempts to resolve conflicts where two matched bounds (rectangles)
// overlap each other. For most conflicts, this algorithm will simply
// discard the match with the lower result score. But in cases where a
// GATE VALVE and a GLOBE VALVE conflict, the algorithm will instead
// compute their matchTemplate score with the original symbol and test
// image (instead of the thinned images). This is because the thinned
// image for GATE and GLOBE valves are almost identical, and comparing
// the result values is too close to be reliable.
// image: the original, non-thinned test image
// symbols: map of symbols
// matches: map of matched symbols
void resolve_all_conflicts(const cv::Mat & image, 
                           const std::map<std::string, Symbol> & symbols,
                           std::map<std::string, std::vector<Match>> & matches)
{
    // Iterate through all combinations of matched symbols
    for (auto & kv1 : matches)
    {
        std::vector<Match> & m1 = kv1.second;
        for (auto & kv2 : matches)
        {
            std::vector<Match> & m2 = kv2.second;

            for (size_t i = 0; i < m1.size(); ++i)
            {
                for (size_t j = 0; j < m2.size(); ++j)
                {
                    // Skip comparing a match against itself
                    if (kv1.first == kv2.first && i == j)
                        continue;

                    // Detect if the bounding rectangles overlap
                    const cv::Rect & r1 = m1[i].bounds();
                    const cv::Rect & r2 = m2[j].bounds();
                    if (!(r1.x + r1.width < r2.x || r1.y + r1.height < r2.y || r1.x > r2.x + r2.width || r1.y > r2.y + r2.height))
                    {
                        // Remove the match with the lower result
                        float m1_result = m1[i].result();
                        float m2_result = m2[j].result();

                        // Special case - GATE and GLOBE valves tend to conflict alot
                        // as their thinned images are almost the same
                        // So rather than just compare their score, do a second pass
                        // comparing the original image and symbol and use that value
                        // to compare
                        if ((m1[i].name() == "GATE VALVE" && m2[j].name() == "GLOBE VALVE") ||
                            (m1[i].name() == "GLOBE VALVE" && m2[j].name() == "GATE VALVE"))
                        {
                            // Get the result for the first image
                            const auto & iter1 = symbols.find(m1[i].name());
                            cv::Mat image_symbol1;

                            // Resize the original symbol image
                            cv::resize(iter1->second.image().clone(), image_symbol1, cv::Size(),
                                m1[i].scale() / 100.0, m1[i].scale() / 100.0);
                            
                            // Rotate the image
                            rotate_image_90n(image_symbol1, image_symbol1, m1[i].rotation());

                            // Get the match result
                            cv::Mat result1;
                            cv::Rect bounds1(m1[i].bounds().x - 1,
                                             m1[i].bounds().y - 1,
                                             image_symbol1.cols,
                                             image_symbol1.rows);
                            cv::Mat img1(image, bounds1);
                            get_matches(img1, image_symbol1, result1);

                            // Override the original match value
                            m1_result = result1.at<float>(0, 0);
                            
                            // Get the result for the second image
                            const auto & iter2 = symbols.find(m2[j].name());
                            cv::Mat image_symbol2;

                            // Resize the original symbol image
                            cv::resize(iter2->second.image().clone(), image_symbol2, cv::Size(),
                                m2[j].scale() / 100.0, m2[j].scale() / 100.0);

                            // Rotate the image
                            rotate_image_90n(image_symbol2, image_symbol2, m2[j].rotation());
                            
                            // Get the match result
                            cv::Mat result2;
                            cv::Rect bounds2(m2[j].bounds().x - 1,
                                             m2[j].bounds().y - 1,
                                             image_symbol2.cols,
                                             image_symbol2.rows);
                            cv::Mat img2(image, bounds2);
                            get_matches(img2, image_symbol2, result2);

                            // Override the original match value
                            m2_result = result2.at<float>(0, 0);
                        }
                        
                        // Compare results, and remove the lower one
                        if (m1_result < m2_result)
                        {
                            m1.erase(m1.begin() + i);
                            --i;
                            if (kv1.first == kv2.first && i < j)
                                --j;
                        }
                        else
                        {
                            m2.erase(m2.begin() + j);
                            --j;
                            if (kv1.first == kv2.first && j < i)
                                --i;
                        }
                    }
                }
            }
        }
    }
}

// Execute the logic for the second challenge
// test_images: a map containing the test image data
// symbols: a map containing the symbol images to match
//          against the test data
void
challenge2(const std::map<std::string, cv::Mat> & test_images,
           const std::map<std::string, Symbol> & symbols)
{
    // Iterate through each of the test images
    for (const auto & kv : test_images)
    {   
        std::cout << "Image: " << kv.first << std::endl;

        // Ensure the results directory is created
        std::ostringstream mkdir_cmd;
        mkdir_cmd << "mkdir -p result/challenge2/" << kv.first;
        system(mkdir_cmd.str().c_str());

        // Convert the image to grayscale
        cv::Mat image_gray;
        cv::cvtColor(kv.second, image_gray, CV_BGR2GRAY);

        // Thin the image
        cv::Mat image_thinned;
        thinning_with_inversion(image_gray, image_thinned);

        std::ostringstream doc_path;
        doc_path << "result/challenge2/" << kv.first << "_thinned.png";
        cv::imwrite(doc_path.str(), image_thinned);

        // Retrive the list of symbol matches
        std::map<std::string, std::vector<Match>> matches;
        std::vector<std::thread> threads;
        std::cout << "\tStarting threads..." << std::endl;
        for (const auto & kv2 : symbols)
        {
            // Create a new thread to process each symbol
            const Symbol & symbol = kv2.second;
            threads.push_back(std::thread(symbol_match_all_handler,
                                          std::cref(image_thinned),
                                          std::cref(symbol),
                                          std::ref(matches[symbol.name()])));
        }

        // Wait for all threads to finish
        for (auto & thread : threads)
            thread.join();
        std::cout << "\tAll threads complete." << std::endl;

        // Resolve conflicts between matched symbols
        std::cout << "\tResolve conflicts...";
        resolve_all_conflicts(image_gray, symbols, matches);
        std::cout << "done!" << std::endl;

        // Save images to disk with matched symbols overlayed
        std::cout << "\tSave images with symbol overlay..." << std::endl;
        cv::Mat image_save_all = kv.second.clone();
        for (const auto & kv2 : symbols)
        {
            const Symbol & symbol = kv2.second;
            // Image to save
            cv::Mat image_save = kv.second.clone();

            // Path of image
            std::ostringstream save_path;
            save_path << "result/challenge2/" << kv.first << "/" << symbol.name() << ".png";

            // Draw rectangles on image
            for (const auto & match : matches[symbol.name()])
            {
                cv::rectangle(image_save, match.bounds(),
                    cv::Scalar(0, 0, 255, 255), 2, 8, 0);

                cv::rectangle(image_save_all, match.bounds(),
                    cv::Scalar(0, 0, 255, 255), 2, 8, 0);
            }

            // Write the image to disk
            cv::imwrite(save_path.str(), image_save);
            std::cout << "\t\tSaved image: " << save_path.str() << std::endl;
        }
        std::ostringstream save_all_path;
        save_all_path << "result/challenge2/" << kv.first << "/all.png";
        cv::imwrite(save_all_path.str(), image_save_all);
        std::cout << "\t\tSaved image: " << save_all_path.str() << std::endl;
        std::cout << "\tdone!" << std::endl;
    }
}

int
main(int argc, char ** argv)
{
    // Get the start time
    time_t start;
    time(&start);

    // Load the test images
    std::map<std::string, cv::Mat> test_images;
    test_images["c2-demo-1"] = cv::imread("data/challenge2/c2-demo-1.png", CV_LOAD_IMAGE_UNCHANGED);
    test_images["c2-demo-2"] = cv::imread("data/challenge2/c2-demo-2.png", CV_LOAD_IMAGE_UNCHANGED);
    
    // Load the symbol sheet
    cv::Mat symbol_sheet = cv::imread("data/challenge2/c2-demo-symbols.png", CV_LOAD_IMAGE_GRAYSCALE);

    // Build the vector of individual symbols from the symbol sheet
    std::map<std::string, Symbol> symbols;
    symbols["GATE VALVE"] = Symbol("GATE VALVE", symbol_sheet, cv::Rect(152, 168, 88, 88), 0.83, cv::Range(30, 50));
    symbols["GLOBE VALVE"] = Symbol("GLOBE VALVE", symbol_sheet, cv::Rect(152, 294, 88, 88), 0.8, cv::Range(30, 50));
    symbols["PLUG VALVE"] = Symbol("PLUG VALVE", symbol_sheet, cv::Rect(152, 418, 88, 88), 0.8, cv::Range(30, 50));
    symbols["BUTTERFLY VALVE"] = Symbol("BUTTERFLY VALVE", symbol_sheet, cv::Rect(152, 546, 88, 88), 0.9, cv::Range(30, 50));
    symbols["BALL VALVE"] = Symbol("BALL VALVE", symbol_sheet, cv::Rect(152, 672, 89, 88), 0.60, cv::Range(30, 50));
    symbols["CHECK VALVE"] = Symbol("CHECK VALVE", symbol_sheet, cv::Rect(152, 820, 89, 92), 0.9, cv::Range(30, 50));
    symbols["NEEDLE VALVE"] = Symbol("NEEDLE VALVE", symbol_sheet, cv::Rect(152, 946, 89, 92), 0.89, cv::Range(30, 50));
    symbols["BARRED TEE"] = Symbol("BARRED TEE", symbol_sheet, cv::Rect(1290, 190, 68, 52), 0.8, cv::Range(30, 50));
    symbols["REDUCER"] = Symbol("REDUCER", symbol_sheet, cv::Rect(1290, 306, 68, 68), 0.75, cv::Range(50, 70));
    symbols["BREAK FLANGES"] = Symbol("BREAK FLANGES", symbol_sheet, cv::Rect(1260, 420, 130, 104), 0.9, cv::Range(15, 35));
    symbols["SPECTACLE BLIND (OPEN)"] = Symbol("SPECTACLE BLIND (OPEN)", symbol_sheet, cv::Rect(1308, 560, 40, 144), 0.8, cv::Range(30, 50));
    symbols["SPECTACLE BLIND (CLOSED)"] = Symbol("SPECTACLE BLIND (CLOSED)", symbol_sheet, cv::Rect(1308, 760, 40, 144), 0.8, cv::Range(30, 50));
    symbols["FILTER"] = Symbol("FILTER", symbol_sheet, cv::Rect(1268, 944, 118, 116), 0.90, cv::Range(30, 50));

    // Save each symbol as an individual image
    system("mkdir -p result/challenge2/symbols");
    for (const auto & kv : symbols)
    {
        const Symbol & symbol = kv.second;
        // Raw image
        std::ostringstream path;
        path << "./result/challenge2/symbols/" << symbol.name() << ".png";
        cv::imwrite(path.str(), symbol.image());

        // Inverted and thinned image
        path.str("");
        path.clear();
        path << "./result/challenge2/symbols/" << symbol.name() << " (thinned).png";
        cv::Mat thinned_symbol;
        thinning_special_with_inversion(symbol.image(), thinned_symbol);
        cv::imwrite(path.str(), thinned_symbol);
    }

    // Do challenge 2
    challenge2(test_images, symbols);

    // Get the end time and calculate the program execution time
    time_t end;
    time(&end);
    double seconds = difftime(end, start);
    std::cout << "Running time: " << (int)(seconds / 60) << "m "
        << (int)seconds % 60 << "s" << std::endl;
}