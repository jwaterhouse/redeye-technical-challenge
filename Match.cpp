#include "Match.h"

//ctor
Match::Match(const std::string &   name,
             const cv::Point2i &   location,
             const cv::Rect &      bounds,
             float                 result,
             double                rotation,
             double                scale) :
             m_name(name),
             m_location(location),
             m_bounds(bounds),
             m_result(result),
             m_rotation(rotation),
             m_scale(scale)
{
}

//dtor
Match::~Match()
{
}

// Getters and setters
const std::string &
Match::name() const
{
    return m_name;
}
void
Match::name(std::string name)
{
    m_name = name;
}

const cv::Point2i &
Match::location() const
{
    return m_location;
}
void
Match::location(const cv::Point2i & location)
{
    m_location = location;
}

const cv::Rect &
Match::bounds() const
{
    return m_bounds;
}
void
Match::bounds(const cv::Rect & bounds)
{
    m_bounds = bounds;
}

float
Match::result() const
{
    return m_result;
}
void
Match::result(float result)
{
    m_result = result;
}

double
Match::rotation() const
{
    return m_rotation;
}
void
Match::rotation(double rotation)
{
    m_rotation = rotation;
}

double
Match::scale() const
{
    return m_scale;
}
void
Match::scale(double scale)
{
    m_scale = scale;
}